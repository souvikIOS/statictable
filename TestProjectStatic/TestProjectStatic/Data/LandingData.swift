//
//  LandingData.swift
//  TestProjectStatic
//
//  Created by Souvik on 09/01/21.
//

import Foundation

class LandingData{
    
    func getAllData() -> RoleListDataModel?{
        
        let data = RoleListDataModel(data: [Roles(roleName: "Role", objective: [Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")]), Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")])]),Roles(roleName: "Role", objective: [Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")]), Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")])]),Roles(roleName: "Role", objective: [Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")]), Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")])]),Roles(roleName: "Role", objective: [Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")]), Objective(name: "Objective", keyResult: [KeyResult(name: "Key Result"), KeyResult(name: "Key Result"), KeyResult(name: "Key Result")])])])
        return data
    }
}
