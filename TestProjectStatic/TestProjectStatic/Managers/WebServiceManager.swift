//
//  WebServiceManager.swift
//  TestProject1
//
//  Created by Souvik on 09/01/21.
//

import Foundation


class WebServiceManager : NSObject{
    typealias WebServiceCompletionBlock = (RoleListDataModel?, Error?) -> Void
    
    public func requestAPI(url: String, httpHeader: [String: String]?, parameter: [String: Any]? = nil, completion: @escaping WebServiceCompletionBlock) {
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeader
        
        if parameter != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameter as Any, options: .prettyPrinted)
            } catch let error {
                print("\(error.localizedDescription)")
                return
            }
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print("\(error!.localizedDescription)")
                
                completion(nil, error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                //                    Utility.log("Error in fetching response")
                completion(nil, nil)
            }
//            print("Response: \(String(decoding: data, as: UTF8.self))")
            let _ = JSONResponseDecoder.decodeFrom(data, returningModelType: RoleListDataModel.self) { (resData, err) in
                if resData != nil{
                    if resData?.code != 200{
                        completion(nil, nil)
                    }else{
                        completion(resData, nil)
                    }
                }else{
                    completion(nil, nil)
                    
                }
            }
        }
        task.resume()
        
        
    }
    
}
