//
//  LandingViewController.swift
//  TestProjectStatic
//
//  Created by Souvik on 09/01/21.
//

import UIKit

class LandingViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var dataModel = LandingData()
    var objData : RoleListDataModel?
    var isHeaderElaborated : Bool = false
    var isCellElaborated : Bool = false
    var elaboratedSection : Int = -1
    var elaboratedRow : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "RecursiveCell", bundle: nil), forCellReuseIdentifier: RecursiveCell.identifier)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.rowHeight = 44.0
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.objData = dataModel.getAllData()
        self.tableView.reloadData()

    }
    

  
}
//MARK : - IBActions -

extension LandingViewController{
    @objc func headerDidClick(_ sender : UIButton){
        isHeaderElaborated = !isHeaderElaborated
        isCellElaborated = false
        if isHeaderElaborated{
            self.elaboratedSection = sender.tag

        }else{
            self.elaboratedSection = -1
        }
        self.tableView.reloadData()
    }
}


//MARK : - Uitableview datasource and delegate -
extension LandingViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objData?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tableView.frame.size.width, height: 40.0))
        let sepView = UIView(frame: CGRect(x: 0.0, y: 39.0, width: self.tableView.frame.size.width, height: 1.0))
        let dotView = UIView(frame: CGRect(x: 0.0, y: 13.0, width: 10.0, height: 10.0))
        dotView.layer.cornerRadius = 5.0
        dotView.layer.masksToBounds = true
        let label = UILabel(frame: CGRect(x: 20.0, y: 0.0, width: self.tableView.frame.size.width, height: 40.0))
        let button = UIButton(frame: view.bounds)
        label.text = self.objData?.data?[section].roleName ?? ""
        label.textColor = UIColor.white
        button.tag = section
        button.addTarget(self, action: #selector(headerDidClick(_:)), for: .touchUpInside)
        view.addSubview(label)
        view.addSubview(button)
        if section == elaboratedSection{
            sepView.backgroundColor = .clear
            dotView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }else{
            sepView.backgroundColor = .white
            dotView.backgroundColor = .clear
        }
        view.addSubview(dotView)
        view.addSubview(sepView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isHeaderElaborated{
            if section == elaboratedSection{
                    return (self.objData?.data?[section].objective!.count)!
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecursiveCell.identifier) as! RecursiveCell
        cell.objectiveData = self.objData?.data?[indexPath.section].objective![indexPath.row]
        if self.isCellElaborated{
            if indexPath.row == self.elaboratedRow{
                cell.isCellClicked = true

            }else{
                cell.isCellClicked = false
            }
        }else{
            self.isCellElaborated = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isCellElaborated = !self.isCellElaborated
        self.elaboratedRow = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: RecursiveCell.identifier, for: indexPath) as! RecursiveCell
        cell.isCellClicked = true
        self.tableView.reloadSections(IndexSet(integersIn: indexPath.section...indexPath.section), with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecursiveCell.identifier, for: indexPath) as! RecursiveCell
        cell.isCellClicked = false
        self.tableView.reloadSections(IndexSet(integersIn: indexPath.section...indexPath.section), with: .automatic)

    }
    
}
