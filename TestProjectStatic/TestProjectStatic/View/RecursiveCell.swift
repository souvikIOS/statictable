//
//  RecursiveCell.swift
//  TestProjectStatic
//
//  Created by Souvik on 09/01/21.
//

import UIKit

class RecursiveCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableViewInner: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableViewInner.register(UINib(nibName: "InnerCell", bundle: nil), forCellReuseIdentifier: InnerCell.identifier)
        self.tableViewInner.dataSource = self
        self.tableViewInner.delegate = self

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var isCellClicked : Bool?{
        didSet{
            if isCellClicked!{
                self.tableHeightConstraint.constant = 132
                self.tableViewInner.reloadData()

            }else{
                self.tableHeightConstraint.constant = 0
                self.tableViewInner.reloadData()
            }
        }
    }
    
    var objectiveData : Objective?{
        didSet {
            labelTitle.text = "\(objectiveData?.name ?? "")"
            self.tableHeightConstraint.constant = 0//CGFloat(44 * (objectiveData?.keyResult!.count)!)
            self.tableViewInner.reloadData()
        }
        
    }

}


//MARK : - Uitableview datasource and delegate -
extension RecursiveCell : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objectiveData?.keyResult?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InnerCell.identifier) as! InnerCell
        cell.keyResultsData = self.objectiveData!.keyResult![indexPath.row]
        return cell
    }
    
    
}


