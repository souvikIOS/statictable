//
//  InnerCell.swift
//  TestProjectStatic
//
//  Created by Souvik on 09/01/21.
//

import UIKit

class InnerCell: UITableViewCell {
    @IBOutlet weak var labelSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var keyResultsData : KeyResult?{
        didSet {
            labelSubTitle.text = "\(keyResultsData?.name ?? "")"
        }
        
    }
}
