//
//  DataModel.swift
//  TestProjectStatic
//
//  Created by Souvik on 09/01/21.
//

import Foundation

// MARK: - NewOrdersListModel
class RoleListDataModel {
    var data: [Roles]?

    init(data: [Roles]?) {
        self.data = data
    }
}

class Roles{
    var roleName: String = "Role"
    var objective: [Objective]?


    init(roleName: String,objective: [Objective]?) {
        self.roleName = roleName
        self.objective = objective
    }
}


// MARK: - Objective
class Objective {
    var name: String = "Objective"
    var keyResult : [KeyResult]?

    init(name: String, keyResult : [KeyResult]?) {
        self.name = name
        self.keyResult = keyResult
    }
}

// MARK: - KeyResult
class KeyResult {
    var name: String = "Key results"
    init(name: String) {
        self.name = name
    }
}

